<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);


?>

<div class="review-block">
	<div class="review-text">
		<div class="review-text-cont">
			<?
			echo $arResult["DETAIL_TEXT"]; ?>
		</div>
		<div class="review-autor">
			<?= $arResult["NAME"] ?>, <?= $arResult["DISPLAY_ACTIVE_FROM"] ?>., <?
			echo $arResult["PROPERTIES"]["POSITION"]["VALUE"] ?>, <?
			echo $arResult["PROPERTIES"]["COMPANY"]["VALUE"] ?>.
		</div>
	</div>
	<div style="clear: both;" class="review-img-wrap">
		<?
		if ($arResult["DETAIL_PICTURE"]["SRC"]): ?>
			<img src="<?= $arResult["DETAIL_PICTURE"]["SRC"] ?>" alt="<?= $arResult["DETAIL_PICTURE"]["ALT"] ?>">
		<?
		else: ?>
			<img src="<?= SITE_TEMPLATE_PATH ?>/img/no_photo.jpg" alt="<?= $arResult["DETAIL_PICTURE"]["ALT"] ?>">
		<?
		endif; ?>
	</div>
</div>
<?
if ($arResult["DISPLAY_PROPERTIES"]["FILIES"]): ?>
	<div class="exam-review-doc">
		<p>Документы:</p>
		<?
		if ($arResult["DISPLAY_PROPERTIES"]["FILIES"]["FILE_VALUE"][0]): ?>
			<?
			foreach ($arResult["DISPLAY_PROPERTIES"]["FILIES"]["FILE_VALUE"] as $file): ?>
				<div class="exam-review-item-doc"><img class="rew-doc-ico"
													   src="<?= SITE_TEMPLATE_PATH ?>/img/icons/pdf_ico_40.png"><a
							href="<?= $file["SRC"] ?>"><?= $file["FILE_NAME"] ?></a></div>
			<?
			endforeach; ?>
		<?
		else: ?>
			<div class="exam-review-item-doc"><img class="rew-doc-ico"
												   src="<?= SITE_TEMPLATE_PATH ?>/img/icons/pdf_ico_40.png"><a
						href="<?= $arResult["DISPLAY_PROPERTIES"]["FILIES"]["FILE_VALUE"]["SRC"] ?>"><?= $arResult["DISPLAY_PROPERTIES"]["FILIES"]["FILE_VALUE"]["FILE_NAME"] ?></a>
			</div>
		<?
		endif; ?>
	</div>
<?
endif; ?>
<hr>
<a href="<?= $arResult["LIST_PAGE_URL"] ?>" class="review-block_back_link"> &larr; К списку отзывов</a>
