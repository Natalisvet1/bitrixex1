<?php
foreach ($arResult["ITEMS"] as $index => $arItem) {
	if ($arItem["PREVIEW_PICTURE"]["ID"]) {
		$arFileTmp = CFile::ResizeImageGet(
			$arItem["PREVIEW_PICTURE"]["ID"],
			array("width" => 68, "height" => 50),
			BX_RESIZE_IMAGE_EXACT,
			false
		);
		$arResult["ITEMS"][$index]["PREVIEW_PICTURE"]["SRC"] = $arFileTmp["src"];
	} else {
		$arResult["ITEMS"][$index]["PREVIEW_PICTURE"]["SRC"] = SITE_TEMPLATE_PATH . "/img/no_photo_left_block.jpg";
	}
}






